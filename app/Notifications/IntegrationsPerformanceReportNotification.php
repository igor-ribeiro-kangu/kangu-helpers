<?php

namespace App\Notifications;

use Carbon\CarbonInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class IntegrationsPerformanceReportNotification extends Notification
{
    use Queueable;

    private array $reportData;
    /**
     * @var CarbonInterface
     */
    private $reportStartPeriod;
    /**
     * @var CarbonInterface
     */
    private $reportEndPeriod;
    /**
     * @var mixed|null
     */
    private $reportWeek;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(array $reportData, array $period, $week = null)
    {
        $this->reportData = $reportData;
        $this->reportStartPeriod = $period[0];
        $this->reportEndPeriod = $period[1];
        $this->reportWeek = $week;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $start = $this->reportStartPeriod->format('d/m/Y');
        $end = $this->reportEndPeriod->format('d/m/Y');

        return (new MailMessage)
                    ->subject("Relatório Performance de Integrações de $start até $end")
                    ->greeting('Olá!')
                    ->line('Segue o relatório de performance de integrações de transportadoras.')
                    ->markdown('mail.integrations-performance-notification', [
                        'report' => $this->reportData,
                        'start_period' => $this->reportStartPeriod,
                        'end_period' => $this->reportEndPeriod,
                        'week' => $this->reportWeek
                    ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
