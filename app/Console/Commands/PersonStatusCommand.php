<?php

namespace App\Console\Commands;

use App\Models\Portal\CaPessoa;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PersonStatusCommand extends Command
{

    use UsesKanguConnection;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'person:status {id : ID da pessoa}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imprime as principais informações sobre uma pessoa no sistema.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $query = $this->buildQuery();

        $person = $query->firstOrFail();

        $this->print($person);

        return self::SUCCESS;
    }

    /**
     * @return Builder
     */
    protected function buildQuery(): Builder {

        return CaPessoa::query()->whereKey($this->argument('id'));
    }

    /**
     * @param CaPessoa|Model $person
     */
    protected function print(CaPessoa $person) {

        $headers = ['Id.', 'Nome', 'Apelido', 'CPF/CNPJ'];

        $this->table($headers, [
            [
                $person->getKey(),
                $person->nome,
                $person->apelido,
                $person->codigo,
            ]
        ], 'box');
    }
}
