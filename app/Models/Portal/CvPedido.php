<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property Collection<TmsPedidoTranspOcorrencia> $automations
 * @property CaOcorrencia $current_event
 * @property Collection<TmsVolume> $volumes
 * @property TmsPedidoTransp $carrier_integration
 * @property CaPessoa $carrier
 * @property CaContrato $contract
 * @property Collection<TmsConciliacao> $conciliation
 */
class CvPedido extends Model
{
    protected $table = 'cv_pedido';

    /**
     * @return HasMany
     */
    public function automations(): HasMany {
        return $this->hasMany(TmsPedidoTranspOcorrencia::class, 'id_pedido');
    }

    /**
     * @return BelongsTo
     */
    public function current_event(): BelongsTo {
        return $this->belongsTo(CaOcorrencia::class, 'id_ocorrencia');
    }

    /**
     * @return BelongsToMany
     */
    public function volumes(): BelongsToMany {
        return $this->belongsToMany(
            TmsVolume::class,
            'tms_volume_pedido',
            'id_pedido',
            'id_volume'
        )->wherePivot('status', 'A');
    }

    /**
     * @return HasOne
     */
    public function carrier_integration(): HasOne {
        return $this->hasOne(
            TmsPedidoTransp::class,
            'id_pedido',
        );
    }

    public function carrier() {
        return $this->belongsTo(CaPessoa::class, 'id_transp');
    }

    public function contract() {
        return $this->belongsTo(CaContrato::class, 'id_contrato');
    }

    public function conciliation() {
        return $this->hasMany(TmsConciliacao::class, 'id_pedido');
    }

    public function origin(): BelongsTo {
        return $this->belongsTo(CvOrigem::class, 'id_origem');
    }

    public function destination_address() {
        return $this->belongsTo(CaEndereco::class, 'id_end_dest');
    }

    public function origin_address() {
        return $this->belongsTo(CaEndereco::class, 'id_endereco');
    }
}
