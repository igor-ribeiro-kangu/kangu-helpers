<?php

    namespace App\Console\Commands;

    use Illuminate\Support\Facades\Config;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;

    trait UsesKanguConnection {

        protected function initialize(InputInterface $input, OutputInterface $output) {
            parent::initialize($input, $output);
            Config::set('database.default', 'kangu');
        }

    }
