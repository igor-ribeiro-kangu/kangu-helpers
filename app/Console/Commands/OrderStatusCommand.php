<?php

namespace App\Console\Commands;

use App\Models\Portal\CvPedido;
use App\Models\Portal\TmsVolume;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OrderStatusCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:status
                                            {order : Número do Pedido}
                                            {--id : Utilizar ID ao invés de número}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imprime status do pedido.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    protected function initialize(InputInterface $input, OutputInterface $output) {
        parent::initialize($input, $output);
        Config::set('database.default', 'kangu');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Analisando 0189103580
        // 0178759830

        $query = $this->makeQuery();

        $order = $query->first();

        if(is_null($order)){
            $this->warn('Pedido não encontrado.');
            return self::SUCCESS;
        }

        $this->displaySimple($order);

        $this->displayFull($order);

        return self::SUCCESS;
    }

    /**
     * @return Builder
     */
    protected function makeQuery(): Builder {
        $query = CvPedido::query();

        if($this->option('id')) {
            $query->whereKey($this->argument('order'));
        } else {
            $query->where('cv_pedido.numero', $this->argument('order'));
        }

        $query->with([
            'automations',
            'current_event:id,descricao',
            'volumes',
            'carrier:id,nome',
            'contract:id,descricao',
            'carrier_integration',
            'conciliation',
            'origin:id,descricao',
            'destination_address',
            'origin_address',
        ]);

        return $query;
    }

    protected function displaySimple(CvPedido $order) {

        $headers = ['ID.', 'Número', 'Pago', 'Última Ocorrência', 'Transportadora', 'Contrato', 'Origem'];

        $rows = [
            [
                $order->getKey(),
                $order->numero,
                $order->pago,
                $order->getRelationValue('current_event') ? $order->current_event->descricao : '',
                $order->getRelationValue('carrier') ? $order->carrier->nome : '',
                $order->getRelationValue('contract') ? $order->contract->descricao : '',
                $order->getRelationValue('origin') ? $order->origin->descricao : 'Portal (Sem Origem)',
            ]
        ];

        $this->info(' Informações Básicas:');

        $this->table($headers, $rows, 'box');
    }

    protected function displayFull(CvPedido $order) {

        $this->displayVolumes($order);

        $this->displayAddresses($order);

        $this->displayModules($order);
    }

    protected function displayVolumes(CvPedido $order) {

        if( ! $order->getRelationValue('volumes')) {
            return;
        }

        $qtd = $order->volumes->count();

        $this->info(" Volumes: $qtd");

        $headers = [
            'ID.',
            'Barcode',
            'Barcode Correio',
            'Serviço',
            'Altura',
            'Largura',
            'Comprimento',
            'Peso',
            'Valor Mercadoria',
        ];

        $rows = $order->volumes->map(function(TmsVolume $volume) {
            return [
                $volume->getKey(),
                $volume->barcode,
                $volume->barcode_correio,
                $volume->servico,
                $volume->altura ?: '<error>Não Informado</error>',
                $volume->largura ?: '<error>Não Informado</error>',
                $volume->comprimento ?: '<error>Não Informado</error>',
                $volume->peso ?: '<error>Não Informado</error>',
                $volume->vlr_produtos ?: '<error>Não Informado</error>',
            ];
        });

        $this->table($headers, $rows, 'box');
    }

    protected function displayModules(CvPedido $order) {

        $this->info(' Módulos:');

        $headers = ['Módulo', 'Info', 'Observações'];

        $rows = [
            $this->getCarrierIntegrationStatus($order),
            $this->getAutomationStatus($order),
            $this->getConciliationStatus($order),
        ];

        $this->table($headers, $rows, 'box');
    }

    protected function getCarrierIntegrationStatus(CvPedido $order): array {

        $data = [
            'Integração Transportadora', 'Não', '-',
        ];

        if($order->getRelationValue('carrier_integration')) {
            $data[1] = $order->carrier_integration->status_integ;
            $data[2] = $order->carrier_integration->mensagem_integ;
        }

        return $data;
    }

    protected function getAutomationStatus(CvPedido $order) {

        $data = ['Automação de Ocorrências', 'Não', '-'];

        if($order->getRelationValue('automations') && $order->automations->isNotEmpty()){
            $count = $order->automations->count();
            $data[1] = 'Sim';
            $data[2] = $count > 1
                ? 'Automação duplicada ids: '. $order->automations->implode('id')
                : ($order->automations->implode('resposta', ' | ') ?: 'Sem respostas.');
        }

        return $data;
    }

    protected function getConciliationStatus(CvPedido $order) {

        $data = ['Conciliação Marketplace', 'Não', '-'];

        if($order->getRelationValue('conciliation') && $order->conciliation->isNotEmpty()) {
            $data[1] = 'Sim';
            $data[2] = $order->conciliation
                ->map(function ($conciliacao) use ($order) {

                    $barcode = $order->volumes->find($conciliacao->id_volume)->barcode;

                    if($conciliacao->dh_conciliacao) {
                        $reconciledIn = Carbon::createFromFormat('Y-m-d H:i:s', $conciliacao->dh_conciliacao)
                        ->formatLocalized('%D às %T');
                        $conciliacao->resumo = "Volume $barcode Conciliado em: $reconciledIn";

                    } else {
                        $conciliacao->resumo = "<error>Volume $barcode não ainda conciliado.</error>";
                    }

                    return $conciliacao;
                })->implode('resumo');
        }

        return $data;
    }

    protected function displayAddresses(CvPedido $order) {

        $addresses = [];

        if($order->getRelation('origin_address')) {
            $addresses[] = [
                '<options=bold>Origem</>',
                $order->origin_address->tipo ?: '<comment>[N/D]</comment>',
                $order->origin_address->logradouro ?: '<comment>[N/D]</comment>',
                $order->origin_address->numero ?: '<comment>[N/D]</comment>',
                $order->origin_address->complemento ?: '<comment>[N/D]</comment>',
                preg_replace('/\D/', '', $order->origin_address->cep),
                $order->origin_address->cidade ?: '<comment>[N/D]</comment>',
                $order->origin_address->uf ?: '<comment>[N/D]</comment>',
                $order->origin_address->lat ?: '<comment>[N/D]</comment>',
                $order->origin_address->lon ?: '<comment>[N/D]</comment>',
            ];
        }

        if($order->getRelation('destination_address')) {

            if(count($addresses)) {
                $addresses[] = new TableSeparator();
            }

            $addresses[] = [
                '<options=bold>Destino</>',
                $order->destination_address->tipo ?: '<comment>[N/D]</comment>',
                $order->destination_address->logradouro ?: '<comment>[N/D]</comment>',
                $order->destination_address->numero ?: '<comment>[N/D]</comment>',
                $order->destination_address->complemento ?: '<comment>[N/D]</comment>',
                preg_replace('/\D/', '', $order->destination_address->cep),
                $order->destination_address->cidade ?: '<comment>[N/D]</comment>',
                $order->destination_address->uf ?: '<comment>[N/D]</comment>',
                $order->destination_address->lat ?: '<comment>[N/D]</comment>',
                $order->destination_address->lon ?: '<comment>[N/D]</comment>',
            ];
        }

        if(count($addresses)) {
            $this->info('Endereços:');
            $this->table([
                'Endereço',
                'Tipo',
                'Logradouro',
                'Número',
                'Complemento',
                'CEP',
                'Cidade',
                'UF',
                'Latitude',
                'Longitude',
            ], $addresses, 'box');
        }
    }
}
