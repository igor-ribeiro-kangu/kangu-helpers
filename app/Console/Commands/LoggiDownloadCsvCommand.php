<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class LoggiDownloadCsvCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'loggi:download-csv {files*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Baixa o arquivo de integração da transportadora Loggi.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $files = $this->argument('files');

        $diskFrom = Storage::disk('loggi');

        $status = [];

        foreach ($files as $file) {
            $from = "BACKUP/$file";
            $to = "loggi/$file";

            if($diskFrom->exists($from)) {
                $content = $diskFrom->get($from);

                Storage::disk('local')->put($to, $content);

                $status[] = [
                    'file' => storage_path($to),
                    'status' => '<info>Arquivo processado.</info>'
                ];
            }else if($diskFrom->exists($file)) {
                $content = $diskFrom->get($file);

                Storage::disk('local')->put($to, $content);

                $status[] = [
                    'file' => storage_path($to),
                    'status' => '<fg=yellow>Arquivo não processado.</>'
                ];
            } else {
                $status[] = [
                    'file' => null,
                    'status' => '<fg=red>Arquivo inexistente.</>'
                ];
            }
        }

        $this->table([
            'Arquivo', 'Status'
        ], array_map('array_values', $status));


        return Command::SUCCESS;
    }
}
