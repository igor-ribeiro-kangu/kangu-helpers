<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OrderFromGroupOrderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:from-group {number : Número do pedido pai.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    protected function initialize(InputInterface $input, OutputInterface $output) {
        parent::initialize($input, $output);
        Config::set('database.default', 'kangu');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $orderNumber = $this->argument('number');

        $orderId = $this->getLinehaulOrderId($orderNumber);

        $childOrders = $this->getChildOrders($orderId);

        if($childOrders->isEmpty()) {
            $this->info('Nenhum pedido filho encontrado para este pedido Linehaul.');
            return self::SUCCESS;
        }

        $this->table(['ID', 'Número'], $childOrders->values());

        return self::SUCCESS;
    }

    /**
     * @param $orderNumber
     * @return int
     */
    protected function getLinehaulOrderId($orderNumber): int {

        return (int) DB::table('cv_pedido')
            ->where('numero', $orderNumber)
            ->where('numero_cli', 'LIKE', 'LH%')
            ->value('id');
    }

    /**
     * @param int $orderId
     * @return Collection
     */
    protected function getChildOrders(int $orderId): Collection {

        return DB::table('cv_pedido')
            ->where('id_ped_grupo', $orderId)
            ->get(['id', 'numero'])
            ->map(fn($order) => (array) $order);
    }
}
