<?php

namespace App\Console\Commands;

use App\Notifications\IntegrationsPerformanceReportNotification;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Carbon\CarbonInterface;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Symfony\Component\Console\Exception\InvalidOptionException;

class ReportIntegrationsPerformanceCommand extends Command
{
    use UsesKanguConnection;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:integrations-performance
                                                            {--start= : Data inicial para filtro do relatório (d/m/Y)}
                                                            {--end= : Data final para filtro do relatório (d/m/Y)}
                                                            {--week= : Número da semana Kangu para construir as datas inicial e final (Caso informado, desconsidera start e end informados).}
                                                            {--email-to=* : Endereço de e-mail para enviar o relatório}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        [$startDate, $endDate] = $this->option('week')
            ? $this->getFilterDatesByKanguWeek()
            : $this->getFilterDatesDefault();

        $performanceByCarrier = $this->performanceByCarrier($startDate, $endDate);

        $this->plotAsTable($performanceByCarrier);

        if($this->option('email-to')) {
            $this->sendAsEmail($performanceByCarrier, [$startDate, $endDate]);
        }

        return Command::SUCCESS;
    }

    /**
     * @return Carbon[]
     */
    private function getFilterDatesDefault(): array {

        $start = $this->option('start');
        $end = $this->option('end');

        if($start XOR $end) {
            throw new InvalidOptionException('As opções --start e --end devem ser informadas exclusivamente juntas.');
        }

        if($start && $end) {
            foreach ([$start, $end] as $date) {
                if( ! Str::match('/\d{2}\/\d{2}\/\d{4}$/', $date)) {
                    throw new InvalidOptionException('As opções --start e --date devem estar no formato dia/mês/ano. Ex: 03/10/1993');
                }
            }
        }else{
            $lastWeekday = new Carbon('last weekday');
            $start = $lastWeekday->format('d/m/Y');
            $end = $lastWeekday->format('d/m/Y');
        }

        $start = Carbon::createFromFormat('d/m/Y', $start)->startOfDay();
        $end = Carbon::createFromFormat('d/m/Y', $end)->endOfDay();

        if($start->isAfter($end)) {
            throw new InvalidOptionException('A data inicial informada não pode estar a frente da data final.');
        }

        $startFormatted = $start->format('d/m/Y H:i:s');
        $endFormatted = $end->format('d/m/Y H:i:s');

        $this->info("Filtros aplicados: de $startFormatted até $endFormatted");

        return [$start, $end];
    }

    /**
     * @return array
     */
    private function performanceByCarrier(CarbonInterface $startDate, CarbonInterface $endDate): array {

        $this->info('Consultando performance por transportadora...');

        $baseQuery = DB::select("SELECT *,
                               ROUND((qtd_integrados_4h / qtd_pedidos) * 100, 2) AS performance_4h
                        FROM (
                                 SELECT STRAIGHT_JOIN tti.transp_nome        AS transportadora,
                                                      COUNT(DISTINCT cp.id) AS qtd_pedidos,
                                                      SUM(TIMESTAMPDIFF(MINUTE, IF(tpt.id_transp = 9900, TIMESTAMP(tv.dh_imp), cpp.dt_col),
                                                                        COALESCE(log_sucesso.dh_inc, tpt.dh_sol)
                                                              ) <= 240
                                                          )                    qtd_integrados_4h
                                 FROM tms_pedido_transp tpt
                                          INNER JOIN cv_pedido cp ON tpt.id_pedido = cp.id
                                          INNER JOIN cv_pedido_prazo cpp ON cp.id = cpp.id_pedido
                                          INNER JOIN tms_transp_armaz tta ON cp.id_transp = tta.id_transp AND tta.id_armaz_resp = cp.id_armaz
                                          INNER JOIN ca_pessoa transp ON tta.id_transp_res = transp.id
                                          INNER JOIN tms_transp_info tti ON tti.id_transp = transp.id
                                          INNER JOIN tms_volume_pedido tvp ON tpt.id_pedido = tvp.id_pedido AND tvp.status = 'A'
                                          INNER JOIN tms_volume tv ON tvp.id_volume = tv.id
                                          LEFT JOIN tms_log_integ_tsp log_1_tentativa
                                                    ON log_1_tentativa.id = (
                                                        SELECT MIN(id)
                                                        FROM tms_log_integ_tsp log
                                                        WHERE tpt.id = log.id_pedido_transp
                                                          AND status IN ('F', 'S')
                                                          AND tipo = 'solicitacao'
                                                    )
                                          LEFT JOIN tms_log_integ_tsp log_sucesso
                                                    ON log_sucesso.id = (
                                                        SELECT MAX(id)
                                                        FROM tms_log_integ_tsp log
                                                        WHERE tpt.id = log.id_pedido_transp
                                                          AND status = 'S'
                                                          AND tipo = 'solicitacao'
                                                    )

                                 WHERE cp.id_ocorrencia != 27                                               -- Etiqueta expirada
                                   AND cp.id_transp NOT IN (9900, 9931, 322088, 435116, 294510, 483825, 483201, 483192,
                                                            294526, 294506, 294490, 984754, 4350862, 36941) -- Apenas TSP MKP
                                   AND (cp.dt_emis BETWEEN ? AND ?)
                                   AND cp.tipo != 'F'
                                   AND tpt.status_integ NOT IN ('R', 'L')
                                   AND (cpp.dt_col IS NOT NULL OR cpp.dt_col != '')
                                 GROUP BY transp.id
                             ) AS result
                        ORDER BY performance_4h DESC ", [$startDate->toDateTimeString(), $endDate->toDateTimeString()]);

        return $baseQuery;
    }

    private function plotAsTable(array $performanceByCarrier) {

        if( ! count($performanceByCarrier)) return;

        $performanceByCarrier = array_map(fn($row) => (array) $row, $performanceByCarrier);

        $headers = [
            'Transportadora',
            'Qtde. Pedidos',
            'Qtde. Integrados em 4h',
            'Performance em 4h',
        ];
        $rows = array_map('array_values', $performanceByCarrier);

        $this->table($headers, $rows);

    }

    private function sendAsEmail(array $performanceByCarrier, array $period)
    {
        $this->info('Enviando relatório por e-mail...');

        $emails = $this->option('email-to');

        $week = $this->option('week') ?: null;

        Notification::route('mail', $emails)
            ->notify(new IntegrationsPerformanceReportNotification($performanceByCarrier, $period, $week));
    }

    private function getFilterDatesByKanguWeek(): array {

        $weekSearch = $this->option('week');

        if( ! is_numeric($weekSearch)) {
            throw new InvalidOptionException('A opção week deve ser um número inteiro');
        }

        $weekSearch = (int) $weekSearch;

        if($weekSearch <= 0) {
            throw new InvalidOptionException('A opção week deve ser um número maior que zero.');
        }

        $daysTimesWeek = $weekSearch * 7;

        $d0 = CarbonImmutable::now()->startOfYear()->startOfDay()->weekday(Carbon::FRIDAY);

        $weekEnds = $d0->addDays($daysTimesWeek - 1);

        $weekStarts = $weekEnds->subDays(6);

        return [$weekStarts->startOfDay(), $weekEnds->endOfDay()];
    }

}
