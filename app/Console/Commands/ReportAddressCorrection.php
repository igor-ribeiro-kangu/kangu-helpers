<?php

namespace App\Console\Commands;

use App\Models\Mongo\CorrectedAddress;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class ReportAddressCorrection extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:address-correction
        {--F|output-folder=#output# : Pasta de destino do relatório}
        {--filename=address-correction-report.csv : Nome do arquivo de destino do relatório}
        {--start-date= : Data inicial para filtro do relatório}
        {--end-date= : Data final para filtro do relatório}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gera relatório de endereços corrigidos via automação de ocorrência de endereço incorreto.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->signature = str_replace(
            '#output#',
            storage_path('app'.DIRECTORY_SEPARATOR.'reports'),
            $this->signature
        );

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        CorrectedAddress::query()->delete();

        $answersQuery = $this->getAnswersQuery();

        $this->importToMongoDB($answersQuery);

        $this->newLine();

        $this->createCsvReport();

        return self::SUCCESS;
    }

    /**
     * @return Builder
     */
    private function getAnswersQuery(): Builder {
        $query = DB::connection('kangu')
            ->table('tms_pedido_transp_ocorrencia', 'tpto')
            ->join('cv_pedido as p', 'tpto.id_pedido', 'p.id')
            ->join('ca_endereco as end_dest', 'p.id_end_dest', 'end_dest.id')
            ->whereNotNull('tpto.resposta')
            ->where('tpto.resposta', '!=', '')
            ->orderByDesc('tpto.id')
            ->select([
                'tpto.id_pedido',
                'p.numero as numero_pedido',
                'tpto.resposta',
                'end_dest.logradouro',
                'end_dest.numero',
                'end_dest.bairro',
                'end_dest.cidade',
                'end_dest.uf',
                'end_dest.cep',
                'end_dest.complemento',
            ]);

        if($this->option('start-date')) {
            $query->whereDate(
                'tpto.dh_resp',
                '>=',
                Carbon::createFromFormat('d/m/Y', $this->option('start-date'))->startOfDay()
            );
        }

        if($this->option('end-date')) {
            $query->whereDate(
                'tpto.dh_resp',
                '<=',
                Carbon::createFromFormat('d/m/Y', $this->option('end-date'))->endOfDay()
            );
        }

        return $query;
    }

    /**
     * @param Builder $answersQuery
     */
    private function importToMongoDB(Builder $answersQuery): void {
        $count = $answersQuery->count();

        $this->info($count . ' registros serão importados.');
        $this->newLine();

        $perPage = 1000;

        if($count < $perPage) $perPage = $count;

        $answersQuery->chunk($perPage, function ($answers, $page) use ($perPage, &$count) {

            $this->info("Importando $perPage registros. Página: $page");

            $progressBar = $this->output->createProgressBar($perPage);

            foreach ($answers as $answer) {

                $correctedAddress = new CorrectedAddress;
                $correctedAddress->order_id = $answer->id_pedido;
                $correctedAddress->order_number = $answer->numero_pedido;
                $newAddressData = json_decode($answer->resposta, true);

                $correctedAddress->old_address = $this->normalizeOldAddressData($answer);
                $correctedAddress->new_address = $this->normalizeNewAddressData($newAddressData);

                $correctedAddress->save();

                $progressBar->advance();
            }

            $progressBar->finish();
            $this->newLine();

            $count = $count > $perPage
                ? $count - $perPage
                : 0;

            $this->info('Restam: ' . $count);
        });

        $this->info('Importação para MongoDB concluída.');
    }

    /**
     * @param $addressData
     * @return array
     */
    protected function normalizeNewAddressData($addressData): array {

        $keys = collect($addressData)->keys()
            ->map(fn($key) => str_replace('end_', '', $key))
            ->toArray();

        $addressData = array_combine($keys, array_values($addressData));

        if(isset($addressData['cep'])) {
            $addressData['cep'] = preg_replace('/\D/', '', $addressData['cep']);
        }

        return [
            'logradouro' => $addressData['logradouro'] ?? '',
            'numero' => $addressData['numero'] ?? '',
            'bairro' => $addressData['bairro'] ?? '',
            'cidade' => $addressData['cidade'] ?? '',
            'uf' => $addressData['uf'] ?? '',
            'cep' => $addressData['cep'] ?? '',
            'complemento' => $addressData['complemento'] ?? '',
            'info_adicional' => $addressData['info_adicional'] ?? '',
        ];
    }

    /**
     * @param $answer
     * @return array
     */
    protected function normalizeOldAddressData($answer): array {

        return [
            'logradouro' => $answer->logradouro,
            'numero' => $answer->numero,
            'bairro' => $answer->bairro,
            'cidade' => $answer->cidade,
            'uf' => $answer->uf,
            'cep' => $answer->cep,
            'complemento' => $answer->complemento,
        ];
    }

    protected function createCsvReport() {

        $this->info('Gerando relatório em CSV...');
        $reportsPath = rtrim($this->option('output-folder'), DIRECTORY_SEPARATOR.'\\');

        $filePath = $reportsPath.DIRECTORY_SEPARATOR.$this->option('filename');

        $fileStream = fopen($filePath, 'w');

        $commonKeys = [
            'cep',
            'logradouro',
            'numero',
            'complemento',
            'bairro',
            'cidade',
            'uf',
        ];

        fputcsv($fileStream, [
            'Número Pedido',
            'CEP Anterior',
            'Logradouro Anterior',
            'Número Anterior',
            'Complemento Anterior',
            'Bairro Anterior',
            'Cidade Anterior',
            'UF Anterior',
            'CEP Corrigido',
            'Logradouro Corrigido',
            'Número Corrigido',
            'Complemento Corrigido',
            'Bairro Corrigido',
            'Cidade Corrigido',
            'UF Corrigido',
            'Informação Adicional Informada',
        ]);

        $count = CorrectedAddress::query()->count();

        $this->info($count . ' registros serão importados.');
        $this->newLine();

        $perPage = 1000;

        if($count < $perPage) $perPage = $count;

        CorrectedAddress::query()->chunkById($perPage, function ($correctedAddresses, $page) use ($commonKeys, &$count, $perPage, $fileStream) {

            $this->info("Adicionando $perPage linhas ao CSV. Página: $page");

            $progressBar = $this->output->createProgressBar($perPage);

            foreach ($correctedAddresses as $correctedAddress) {

                $oldAddress = $correctedAddress->old_address;
                $newAddress = $correctedAddress->new_address;

                $line = [$correctedAddress->order_number];

                foreach ($commonKeys as $key) {
                    $line[] = $oldAddress[$key];
                }
                foreach ($commonKeys as $key) {
                    $line[] = $newAddress[$key];
                }

                $line[] = $newAddress['info_adicional'];

                fputcsv($fileStream, $line);

                $progressBar->advance();

            }

            $progressBar->finish();
            $this->newLine();

            $count = $count > $perPage
                ? $count - $perPage
                : 0;

            $this->info('Restam: ' . $count);
        });

    }
}
