<?php

namespace App\Console\Commands;

use App\Notifications\BackorderPredictionReportNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class BackorderPredictionReportCommand extends Command
{
    use UsesKanguConnection;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:backorder-prediction {--to=* : E-mail do destinatário do relatório.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Relatório de predição de entrega para pedidos atrasados.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $orders = $this->getOrders();

        if( ! count($orders)) {
            $this->info('Consulta não retornou resultados.');
            return self::SUCCESS;
        }

        $filePath = $this->generateCsv($orders);

        if(empty($recipients = $this->option('to'))) {

            $this->info('Relatório emitido com sucesso: '. $filePath);

            return self::SUCCESS;
        }

        $this->sendEmail($filePath, $recipients);

        return self::SUCCESS;
    }

    public function getSqlQuery(): string {
        return "/*report*/
        SELECT p.numero                                 as numero_pedido,
                       transp.apelido                           as transportadora,
                       xd_end.uf                                as uf_origem,
                       xd_end.cidade                            as cidade_origem,
                       dest_end.uf                              as uf_destino,
                       dest_end.cidade                          as cidade_destino,
                       DATE_FORMAT(cpp.dt_prev_ent, '%d/%m/%Y') as data_prevista_de_entrega,
                       DATE_FORMAT(cpp.dt_ent, '%d/%m/%Y')      as data_de_entrega,
                       DATE_FORMAT(cpp.dt_max_ent, '%d/%m/%Y')  as data_prevista_algoritmo,
                       cpp.dias_max_ent                         as dias_previstos_algoritmo
                FROM cv_pedido_prazo cpp
                         INNER JOIN cv_pedido p on cpp.id_pedido = p.id
                         INNER JOIN ca_pessoa transp on p.id_transp = transp.id
                         INNER JOIN ca_pessoa xd on p.id_armaz = xd.id
                         INNER JOIN ca_endereco xd_end on xd.id_endereco = xd_end.id
                         INNER JOIN ca_endereco dest_end on p.id_end_dest = dest_end.id

                WHERE cpp.dt_max_ent IS NOT NULL
                  AND cpp.dias_max_ent IS NOT NULL";
    }

    protected function generateCsv(array $orders) {

        $this->info('Gerando relatório em CSV...');

        $headers = [
            'Número Pedido',
            'Transportadora',
            'UF Origem',
            'Cidade Origem',
            'UF Destino',
            'Cidade Destino',
            'Data Prevista de Entrega',
            'Data de Entrega',
            'Data Prevista Algoritmo',
            'Dias Previstos Algoritmo',
        ];

        $filePath = storage_path('/backorder-prediction-report.csv');

        $fileStream = fopen($filePath, 'w');

        fputcsv($fileStream, $headers);

        $bar = $this->output->createProgressBar(count($orders));

        foreach ($orders as $order) {

            fputcsv($fileStream, [
                $order->numero_pedido,
                $order->transportadora,
                $order->uf_origem,
                $order->cidade_origem,
                $order->uf_destino,
                $order->cidade_destino,
                $order->data_prevista_de_entrega,
                $order->data_de_entrega,
                $order->data_prevista_algoritmo,
                $order->dias_previstos_algoritmo,
            ]);

            $bar->advance();
        }

        fclose($fileStream);

        $bar->finish();

        $this->newLine();

        return $filePath;
    }

    protected function sendEmail(string $filePath, array $recipients) {

        $to = implode(',', $recipients);
        $this->info('Enviando relatório por e-mail para: '.$to);
        Notification::route('mail', $recipients)
            ->notify(new BackorderPredictionReportNotification($filePath));
    }

    /**
     * @return array
     */
    private function getOrders(): array {
        $this->info('Emitindo relatório...');
        $query = $this->getSqlQuery();

        $orders = DB::select($query);
        return $orders;
    }
}
