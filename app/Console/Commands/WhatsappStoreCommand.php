<?php

namespace App\Console\Commands;

use App\Models\Messages;
use Illuminate\Console\Command;

class WhatsappStoreCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wpp:store';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $message = new Messages;

        $message->response = [
            'Nome' => 'Igor',
            'Sobrenome' => 'Ribeiro'
        ];

        $message->save();

        return self::SUCCESS;
    }
}
