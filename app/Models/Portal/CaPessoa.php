<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CaPessoa extends Model
{
    protected $table = 'ca_pessoa';
}
