<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TmsPedidoTranspOcorrencia extends Model
{
    protected $table = 'tms_pedido_transp_ocorrencia';
}
