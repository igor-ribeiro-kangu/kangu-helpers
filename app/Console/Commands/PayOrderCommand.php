<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PayOrderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:pay
                                      {number : Identificador do pedido. Padrão é número de pedido.}
                                      {--id : Interpreta os pedidos como ID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Paga um pedido';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    protected function initialize(InputInterface $input, OutputInterface $output) {
        parent::initialize($input, $output);
        Config::set('database.default', 'kangu');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $this->validateArguments();

            $orderNumber = $this->argument('number');

            $column = $this->option('id') ? 'id' : 'numero';

            $this->warn("Utilizando coluna $column em cv_pedido.");

            if($this->confirm("Tem certeza que deseja definir o pedido $orderNumber como pago?")) {
                $this->payOrder($orderNumber);
            }

            return $this::SUCCESS;
        } catch (\InvalidArgumentException $e) {
            $this->error($e->getMessage());
            return $this::INVALID;
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return $this::FAILURE;
        }
    }

    protected function validateArguments() {

        if( ! is_numeric($this->argument('number'))) {
            throw new \InvalidArgumentException('O argumento number deve estar no formato numérico.');
        }

        if(strlen($this->argument('number')) < 6) {
            throw new \InvalidArgumentException('O argumento number deve conter mais de 6 caracteres. ');
        }
    }

    /**
     * @param $orderNumber
     * @return Builder
     */
    protected function getQuery($orderNumber): Builder {
        $query = DB::table('cv_pedido');

        if($this->option('id')) {
            $query->where('id', $orderNumber);
        } else {
            $query->where('numero', $orderNumber);
        }

        return $query;
    }

    /**
     * @param $orderNumber
     * @throws Exception
     */
    protected function payOrder($orderNumber) {
        $query = $this->getQuery($orderNumber);

        if($count = $query->count() != 1) {
            throw new Exception("O pedido informado retorna $count registros.");
        }

        $query->update([
            'pago' => 'S',
            'id_ocorrencia' => 6
        ]);
    }
}
