<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TmsConciliacao extends Model
{
    protected $table = 'tms_conciliacao';
}
