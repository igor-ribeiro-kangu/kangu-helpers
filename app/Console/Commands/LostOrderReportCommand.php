<?php

namespace App\Console\Commands;

use App\Notifications\BackorderPredictionReportNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

class LostOrderReportCommand extends Command
{

    use UsesKanguConnection;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:lost-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Relatório de Pedidos com ocorrências de extravio.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $orders = $this->getOrders();

        if( ! count($orders)) {
            $this->info('Consulta não retornou resultados.');
            return self::SUCCESS;
        }

        $filePath = $this->generateCsv($orders);

        $this->sendEmail($filePath);

        return self::SUCCESS;
    }

    public function getSqlQuery(): string {
        return "
        SELECT p.id                                     as 'Pedido ID',
       p.numero                                 as 'Pedido Número',
       DATE_FORMAT(cpp.dt_prev_ent, '%d/%m/%Y') as PPE
       ,IF(EXISTS(
                  SELECT TRUE
                  FROM tms_eventos te
                  WHERE te.id_ocorrencia IN
                        (214, 914, 9, 891, 892, 117, 118, 899, 85, 177, 44, 893, 894, 998, 26, 912, 42, 913, 895)
                    AND te.id_pedido = p.id
              ), 'Sim', 'Não')                  as 'Teve Ocorrência Transportadora?'
        ,IF(EXISTS(
                  SELECT TRUE
                  FROM tms_ocor_baixa tob
                  WHERE tob.id_ocorrencia IN
                        (214, 914, 9, 891, 892, 117, 118, 899, 85, 177, 44, 893, 894, 998, 26, 912, 42, 913, 895)
                    AND tob.id_pedido = p.id
              ), 'Sim', 'Não')                  as 'Teve Baixa Manual?'

FROM cv_pedido p
         INNER JOIN cv_pedido_prazo cpp on p.id = cpp.id_pedido

WHERE p.status_oper NOT IN ('E', 'P')
  AND p.status NOT IN ('C', 'D')
  AND cpp.dt_ent IS NULL
  AND p.dt_disp IS NULL

  AND (
        EXISTS(
                SELECT TRUE
                FROM tms_eventos te
                WHERE te.id_ocorrencia IN
                      (7, 9,  26, 42, 44, 46, 85, 117, 118,  177, 214, 883, 891, 892, 893, 894, 895, 899, 912, 913, 914, 998)
                  AND te.id_pedido = p.id
            )
        OR EXISTS(
                SELECT TRUE
                FROM tms_ocor_baixa tob
                WHERE tob.id_ocorrencia IN
                      (7, 9,  26, 42, 44, 46, 85, 117, 118,  177, 214, 883, 891, 892, 893, 894, 895, 899, 912, 913, 914, 998)
                  AND tob.id_pedido = p.id
            )
    )
  AND p.id_ocorrencia NOT IN (7, 9,  26, 42, 44, 46, 85, 117, 118,  177, 214, 883, 891, 892, 893, 894, 895, 899, 912, 913, 914, 998)";
    }

    protected function generateCsv(array $orders) {

        $this->info('Gerando relatório em CSV.');

        $headers = ['Pedido ID', 'Pedido Número', 'PPE', 'Teve Ocorrência Transportadora?', 'Teve Baixa Manual?'];

        $filePath = storage_path('/pedidos-com-extravios.csv');

        $fileStream = fopen($filePath, 'w');

        fputcsv($fileStream, $headers);

        $bar = $this->output->createProgressBar(count($orders));

        foreach ($orders as $order) {
            $order = (array) $order;

            fputcsv($fileStream, [
                $order[$headers[0]],
                $order[$headers[1]],
                $order[$headers[2]],
                $order[$headers[3]],
                $order[$headers[4]],
            ]);
            $bar->advance();
        }

        fclose($fileStream);

        $bar->finish();

        $this->newLine();

        return $filePath;
    }

    protected function sendEmail($filePath) {

        $to = 'lais.fernandes@kangu.com.br';
        $this->info('Enviando relatório por e-mail para: '.$to);
        Notification::route('mail', $to)
            ->notify(new BackorderPredictionReportNotification($filePath));
    }

    /**
     * @return array
     */
    private function getOrders(): array {
        $this->info('Consultando pedidos...');
        $query = $this->getSqlQuery();

        $orders = DB::select($query);
        return $orders;
    }
}
