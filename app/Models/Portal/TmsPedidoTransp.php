<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TmsPedidoTransp extends Model
{
    protected $table = 'tms_pedido_transp';

    public function getStatusIntegAttribute($value) {
        $values = [
            'A' => 'Aguardando',
            'S' => 'Solicitado',
            'C' => 'Coletado',
            'E' => 'Entregue',
            'R' => 'Removido',
            'L' => 'Log',
        ];

        return array_key_exists($value, $values)
            ? $values[$value]
            : $value;
    }
}
