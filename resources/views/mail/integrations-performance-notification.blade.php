@component('mail::message')
    # Performance de Integrações até 4h

@component('mail::panel')
    @if(is_numeric($week))
    Week {{$week}}, de {{$start_period->format('d/m/Y')}} até {{$end_period->format('d/m/Y')}}
    @else
    Período de {{$start_period->format('d/m/Y')}} até {{$end_period->format('d/m/Y')}}
    @endif
@endcomponent

@component('mail::table')
    |Transportadora | Total | Integrados em 4h | Performance |
    | :------------ | ----: | ---------------: | ----------: |
    @foreach($report as $row)
        | {{ $row->transportadora }} | {{ $row->qtd_pedidos }} | {{ $row->qtd_integrados_4h }} | {{ $row->performance_4h }}% |
    @endforeach
@endcomponent

    Atenciosamente,
    {{ config('app.name') }}
@endcomponent
