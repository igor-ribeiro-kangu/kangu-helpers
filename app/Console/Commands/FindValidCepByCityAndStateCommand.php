<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FindValidCepByCityAndStateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'address:cep-by-city-state';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected \Illuminate\Database\ConnectionInterface $db;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    protected function initialize(InputInterface $input, OutputInterface $output) {
        parent::initialize($input, $output);
        $this->db = DB::connection('kangu');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $filePath = storage_path('app/cidades-estados.csv');
        $filePathParsed = storage_path('app/cidades-estados-parsed.csv');

        $this->info('Removendo arquivos anteriores.');
        @unlink($filePathParsed);

        $this->info("Lendo arquivo $filePath.");
        $countRows = ($this->countFileRows($filePath) - 1);
        $this->info("$countRows linhas serão lidas.");

        $progressBar = $this->output->createProgressBar($countRows);

        $streamRead = fopen($filePath, 'r');
        $headers = $this->trimArrayValues(fgetcsv($streamRead, 0, ';'));

        $streamWrite = fopen($filePathParsed, 'w');

        fputcsv($streamWrite, [...$headers, 'CEP', 'CEP Geral'], ';');

        while (($line = fgetcsv($streamRead, 0, ';')) !== false) {

            $line = $this->trimArrayValues($line);

            [$city, $state] = $line;
            $cep = '';
            $cepGeral = false;
            if($city && $state) {
                $cityAndStateIds = $this->getCityAndStateIds($city, $state);
                if( ! is_null($cityAndStateIds)) {
                    [$cityId, $stateId] = $cityAndStateIds;
                    $cep = $this->getCepFromCityAndStateAddress($cityId, $stateId);
                    if( $cepGeral = ! $cep) {
                        $cep = $this->getCepFromCityAndState($cityId, $stateId);
                    }
                }
            }
            fputcsv($streamWrite, [$city, $state, $cep, $cepGeral ? 'Sim' : 'Não'], ';');

            $progressBar->advance();
        }

        $progressBar->finish();
        $this->output->newLine();

        $this->info('Finalizado com sucesso.');
        $this->info("Arquivo de saída: $filePathParsed");

        return self::SUCCESS;
    }

    protected function countFileRows(string $filePath) : int {

        $wcOutput = exec("wc -l $filePath");
        [$countRows] = explode(' ', $wcOutput);

        return (int) $countRows;
    }

    protected function trimArrayValues(array $data): array {
        return array_map(fn($value) => trim(trim($value, '﻿')), $data);
    }

    protected function getCityAndStateIds($city, $state) : ?array {

        $query = 'SELECT cc.id as id_cidade, ce.id as id_estado
                    FROM ca_cidade cc
                        INNER JOIN ca_estado ce on cc.id_estado = ce.id
                    WHERE ce.uf = ? AND cc.nome = ? LIMIT 1';

        $result = $this->db->selectOne($query, [$state, $city]);

        if( ! $result) return null;

        return [$result->id_cidade, $result->id_estado];
    }

    protected function getCepFromCityAndStateAddress($cityId, $stateId) : ?string {

        $query = 'SELECT DISTINCT end.cep
                        FROM ca_endereco end
                                 INNER JOIN ca_cidade cc ON end.id_cidade = cc.id
                                 INNER JOIN ca_estado ce ON cc.id_estado = ce.id
                        WHERE end.cep != \'\'
                          AND end.id_pais = ?
                          AND cc.id = ?
                          AND ce.id = ?';

        $result = $this->db->selectOne($query, [1, $cityId, $stateId]);

        if( ! $result) return null;

        return $result->cep;
    }

    protected function getCepFromCityAndState($cityId, $stateId) {

        $query = 'SELECT cc.cep
                    FROM ca_cidade cc
                             INNER JOIN ca_estado ce ON cc.id_estado = ce.id
                    WHERE cc.id = ?
                      AND ce.id = ?';

        $result = $this->db->selectOne($query, [$cityId, $stateId]);

        if( ! $result) return null;

        return $result->cep;
    }
}
